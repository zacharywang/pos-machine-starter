package pos.machine;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<Item> database = ItemsLoader.loadAllItems();
        Map<String, Integer> quantity = getProductsQuantity(barcodes);
        List<String> items = calculateSubtotal(database, quantity);
        int total = calculateTotal(items);
        return generateReceiptText(items, total);
    }

    //得到商品的数量
    private Map<String, Integer> getProductsQuantity(List<String> barcodes) {
        Map<String, Integer> quantity = new LinkedHashMap<>();
        for (String barcode : barcodes) {
            quantity.put(barcode, quantity.getOrDefault(barcode, 0) + 1);
        }
        return quantity;
    }

    //计算每个商品的总价
    private List<String> calculateSubtotal(List<Item> database, Map<String, Integer> quantity) {
        List<String> items = new ArrayList<>();
        for (String barcode : quantity.keySet()) {
            for (Item item : database) {
                if (item.getBarcode().equals(barcode)) {
                    int qty = quantity.get(barcode);
                    int subtotal = item.getPrice() * qty;
                    generateLine(items, item, qty, subtotal);
                    break;
                }
            }
        }
        return items;
    }

    //一行的字符串生成
    private void generateLine(List<String> items, Item item, int qty, int subtotal) {
        items.add(
                String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)",
                        item.getName(),
                        qty,
                        item.getPrice(),
                        subtotal)
        );
    }

    //计算所有商品总价
    private int calculateTotal(List<String> items) {
        int total = 0;
        for (String item : items) {
            total = calculateTotalPrice(total, item);
        }
        return total;
    }

    //分割字符串处理后计算总值
    private int calculateTotalPrice(int total, String item) {
        String[] parts = item.split(":");
        String subtotalStr = parts[parts.length - 1].trim().split(" ")[0];
        total += Integer.parseInt(subtotalStr);
        return total;
    }

    //生成最终票据
    private String generateReceiptText(List<String> items, int total) {
        return "***<store earning no money>Receipt***\n" +
                String.join("\n", items) + "\n" +
                "----------------------\n" +
                String.format("Total: %d (yuan)\n", total) +
                "**********************";
    }
}
